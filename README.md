# Templates

This is simply a collection of boilerplate or template files used when setting up a new
project or repository.

## Acknowledgement

This README brought to you courtesy of the [Acme Readme Corp.][1]

## Files & Types

### Types
<!--- Start of inline HTML for table -->
<table style="width:45%" cellspacing="2" cellpadding="5" border="0" bgcolor="#000000">
  <tr bgcolor="#ccc">
    <th>Ext</th>
    <th>Type</th>
    <th>Notes</th>
  </tr>
  <tr bgcolor="#fff">
    <td align="center">.md</td>
    <td>markdown</td>
    <td>Text file containing markdown formatting.</td>
  </tr>
  <tr bgcolor="#fff">
    <td align="center">.mdtp</td>
    <td>markdown template</td>
    <td>Template text file containing markdown formatting. Copy and rename to .md and edit.</td>
  </tr>
  <tr bgcolor="#fff">
    <td align="center">.shtp</td>
    <td>bash template</td>
    <td>Template bash script file.  Copy and rename to .sh and edit.</td>
  </tr>
</table>
<!--- End of inline HTML for table -->

### Files

1. LICENSE.md - GPLv3 license file, simply add to a new repository.
1. README.md - Read me file for this repository.
1. README.mdtp - Boilerplate read me template, copy, rename and edit as required.

See [GitLab Markdown][2] for details about markdown formatting.

## Installation & Usage

Simply copy the required files into the new repository and edit it as required.

## Reporting bugs

Please use the [GitLab issue tracker][3] for any bugs or feature suggestions.

## Contributing

1. [Fork the project][4]
2. Create your feature branch: ***`git checkout -b my-new-feature`***
3. Commit your changes: ***`git commit -am 'Add some feature'`***
4. Push to the branch: ***`git push origin my-new-feature`***
5. [Submit a merge request][5]

Contributions must be licensed under the GNU GPLv3.
The contributor retains the copyright.

## Credits

See [Acknowledgement](##-Acknowledgement)

## License

This project is licensed under the GNU General Public License, v3. A copy of this license
is included in the file [LICENSE.md](LICENSE.md).

  [1]: https://gist.github.com/zenorocha/4526327
  [2]: https://docs.gitlab.com/ee/user/markdown.html
  [3]: https://gitlab.com/WhoWantsToKnow/Templates/-/issues
  [4]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork
  [5]: https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html
